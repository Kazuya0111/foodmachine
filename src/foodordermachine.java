import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodordermachine {
    private JPanel root;
    private JButton tempuraButton;
    private JButton ramenButton;
    private JButton udonButton;
    private JTextPane textPane1;

    void order(String food){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you to order "+food+" ?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if(confirmation == 0){
            textPane1.setText("Order for "+food+" received.");
            JOptionPane.showMessageDialog(null,
                    "Order for "+food+" received.");
        }
    }

    public foodordermachine() {
        tempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
               order("tempura");
            }
        });
        ramenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("ramen");
            }
        });
        udonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("udon");
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodordermachine");
        frame.setContentPane(new foodordermachine().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}
