import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class foodorder2 {
    private JPanel root;
    private JButton specialButton;
    private JButton ramenButton;
    private JButton spicyButton;
    private JButton mazesobaButton;
    private JButton tsukemenButton;
    private JButton takeoutButton;
    private JTextPane List;
    private JButton checkOutButton;
    private JLabel Total;
    int sum = 0;

    public void order(String food, String pay, int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
        "Would you like to order" +food+ "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if(confirmation == 0){
            String currentText = List.getText();
            List.setText(currentText + food + " " + pay + "\n");
            JOptionPane.showMessageDialog(null,
                    "Thank you for ordering " + food + "! It will be served as soon as possible." );
            List.getText();
            sum = sum + price;
            Total.setText(+ sum + " yen");

        }
    }

    public foodorder2() {
        Total.setText("Total 0 yen");
        List.getText();

        specialButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Special ramen","1150yen",1150);
            }
        });

        specialButton.setIcon(new ImageIcon(
                this.getClass().getResource("Special.png")));

        ramenButton.addActionListener(new ActionListener() {
            @Override
            public  void actionPerformed(ActionEvent e) {
                order("Ramen","800yen",800);
            }
        });
        ramenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Ramen.png")));

        spicyButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Spicy ramen","900yen",900);
            }
        });
        spicyButton.setIcon(new ImageIcon(
                this.getClass().getResource("Spicy.png")));

        mazesobaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Mazesoba","900yen",900);
            }
        });
        mazesobaButton.setIcon(new ImageIcon(
                this.getClass().getResource("Mazesoba.png")));

        tsukemenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tsukenmen","950yen",950);
            }
        });
        tsukemenButton.setIcon(new ImageIcon(
                this.getClass().getResource("Tsukemen.png")));

        takeoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Take out","800yen",800);
            }
        });
        takeoutButton.setIcon(new ImageIcon(
                this.getClass().getResource("Takeout.png")));


        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int checkout = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Checkout Confirmation",
                        JOptionPane.YES_NO_OPTION);

                if (checkout == 0){
                    JOptionPane.showMessageDialog(null,
                            "Thank you. The total price is " +sum + "yen.",
                            "Message",
                            JOptionPane.CLOSED_OPTION);
                    sum = 0;
                    List.setText("");
                    Total.setText("Total 0 yen");
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("foodorder2");
        frame.setContentPane(new foodorder2().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }
}





